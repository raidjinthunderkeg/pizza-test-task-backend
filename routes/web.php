<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Resources\Pizzas as PizzaResource;
use App\Pizzas;

Route::get('/', function () {
    return view('react');
    //return view('welcome');
});

Route::get('/{any}', function () {
    return view('react');
    //return view('welcome');
});


Route::get('/react', function () {
    return view('react');
});



