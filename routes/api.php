<?php

use App\Http\Resources\Pizzas as PizzaResource;
use App\Order;
use App\Pizzas;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/pizza/get', function () {
    return PizzaResource::collection(Pizzas::all());
});

Route::post('/order/create', function (Request $request) {
    $requestPayload = $request->post();
    $data = array_shift($requestPayload );
    return Order::create( json_decode( $data, true ) );
});
