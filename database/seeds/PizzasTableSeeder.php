<?php

use App\Order;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class PizzasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pizzas')->insert([
            'name'          => 'Supreme',
            'description'   => 'Traditional crust brushed with garlic butter and topped with tomato sauce, 100% real cheese, pepperoni, beef, sausage, red onions, green peppers and mushrooms.',
            'price_usd'     => 9,
            'image_url'     => 'supreme_pizza.png'
        ]);

        DB::table('pizzas')->insert([
            'name'          => 'Meat Eater',
            'description'   => 'Traditional crust brushed with garlic butter and topped with tomato sauce, 100% real cheese, pepperoni, ham, beef and sausage.',
            'price_usd'     => 7,
            'image_url'     => 'meat_eater_pizza.png'
        ]);

        DB::table('pizzas')->insert([
            'name'          => 'Veggie',
            'description'   => 'Traditional crust brushed with garlic butter and topped with tomato sauce, 100% real cheese, red onions, green peppers, mushrooms and black olives.',
            'price_usd'     => 5.5,
            'image_url'     => 'pizza_veggie.png'
        ]);

        DB::table('pizzas')->insert([
            'name'          => 'Pepperoni',
            'description'   => 'Traditional crust brushed with garlic butter and topped with tomato sauce, 100% real cheese and pepperoni.',
            'price_usd'     => 6,
            'image_url'     => 'pepperoni_pizza.png'
        ]);

        DB::table('pizzas')->insert([
            'name'          => 'Alfredo',
            'description'   => 'Traditional crust brushed with garlic butter and topped with creamy Alfredo sauce and 100% real cheese.',
            'price_usd'     => 5.5,
            'image_url'     => 'pizza_alfredo.png'
        ]);

        DB::table('pizzas')->insert([
            'name'          => 'BBQ Pork',
            'description'   => 'Traditional crust brushed with garlic butter and topped sweet and tangy honey BBQ sauce, 100% real cheese, and deliciously seasoned pulled pork.',
            'price_usd'     => 7.5,
            'image_url'     => 'pizza_bbq_pork.png'
        ]);

        DB::table('pizzas')->insert([
            'name'          => 'Zesty Ham',
            'description'   => 'Traditional crust brushed with garlic butter and topped with Zesty Parmesan Ranch sauce, 100% real cheese, 100% real cheddar and ham.',
            'price_usd'     => 9,
            'image_url'     => 'pizza_zesty_ham.png'
        ]);

        DB::table('pizzas')->insert([
            'name'          => 'Cheese',
            'description'   => 'Traditional crust brushed with garlic butter and topped with tomato sauce and 100% real cheese.',
            'price_usd'     => 4,
            'image_url'     => 'pizza_cheese.png'
        ]);

    }
}
