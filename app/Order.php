<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['client_name', 'client_phone', 'client_email',
        'items_ordered', 'delivery_type', 'delivery_address_1', 'delivery_address_2', 'status' ];

}
