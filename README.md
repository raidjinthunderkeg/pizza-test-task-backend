## Pizza Test Task

This is a simple project where you can order some pizzas.

## DEMO
Demo project can be accesed at https://pizza-test-task.herokuapp.com/

## Tech Stack
Back-end: Laravel 6.2 (this repository) https://laravel.com/docs/6.x/<br />
Front-end: ReactJS<br />
Database: MySQL

## Build and Deploy

1. Download Laravel repository (this).
2. Install dependencies  `composer install`
3. Create .env file (from .env.example) and set env vars.
4. Migrate and seed your database using `php artisan migrate --seed`
4. Copy (with replace) front-end bundle from [second repository](https://gitlab.com/raidjinthunderkeg/pizza-test-task-front-end) into /public folder. Bundled files located at /build folder and can be built using `yarn build`
5. Rename index.html in /public to react.php and put it into /resources/views folder
6. Deploy your app into any platform.



